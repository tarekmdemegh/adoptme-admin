import {
  SET_FAVORIES,
  GET_POST,
  GET_POST_DETAILS,
  LIST_FAVORIES,
  SET_NOTIFS,
  LIST_NOTIFS,
} from "./actionsType";
import { uiStartLoading, uiStopLoading } from "./index";
import API from "./../../env.json";

export const getNotifs = (data) => {
  const { userId } = data;
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/getNotification`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        postUserId: userId,
      }),
    })
      .catch((err) => {
        //("err", err);
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            dispatch(getNotifsR(data));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};

export const getNotifsR = (value) => {
  return {
    type: LIST_NOTIFS,
    ListNotifs: value,
  };
};

//

export const setNotifs = (data) => {
  const { userId, postId, postUserId } = data;
  console.log("data", data);
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/setNotification`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId,
        postId,
        postUserId,
      }),
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            dispatch(setNotifsR(data));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};

export const setNotifsR = (value) => {
  return {
    type: SET_NOTIFS,
    setNotifs: value,
  };
};
