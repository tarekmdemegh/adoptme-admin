import {
  SET_FAVORIES,
  GET_POST,
  GET_POST_DETAILS,
  LIST_FAVORIES,
} from "./actionsType";
import { uiStartLoading, uiStopLoading } from "./index";
import API from "./../../env.json";

export const getFavories = (data) => {
  const { userId, type } = data;
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/getFavories`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId,
        type,
      }),
    })
      .catch((err) => {
        //("err", err);
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            dispatch(getFavoerisD(data));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};

export const getFavoerisD = (value) => {
  return {
    type: LIST_FAVORIES,
    ListFavoeris: value,
  };
};

//

export const setFavories = (data) => {
  const { userId, postId, type } = data;
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/setFavories`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId,
        postId,
        type,
      }),
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            dispatch(setFavoeris(data));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};

export const setFavoeris = (value) => {
  return {
    type: SET_FAVORIES,
    setFavoeris: value,
  };
};
