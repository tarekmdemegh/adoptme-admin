import { SET_POST, GET_POST, GET_POST_DETAILS } from "./actionsType";
import { uiStartLoading, uiStopLoading } from "./index";
import API from "./../../env.json";

export const postDetails = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/getPost-detail/${data.id}`, {
      method: "get",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            dispatch(setpostDetails(data));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};
export const setpostDetails = (value) => {
  return {
    type: GET_POST_DETAILS,
    getPostDetails: value,
  };
};
export const setpostDetailsReset = (value) => {
  return {
    type: GET_POST_DETAILS,
    getPostDetails: value,
  };
};
export const posts = (data) => {
  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/getPostsClient/${data.id}`, {
      method: "get",
      headers: {
        "content-type": "multipart/form-data",
      },
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            let arr = [];
            data.data.map((e) => {
              if (e._id && e.userId && e.images.length > 0) arr.push(e);
            });
            //("arr.length", arr.length);
            dispatch(getPosts(arr));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};
export const getPosts = (value) => {
  return {
    type: GET_POST,
    getPosts: value,
  };
};

export const setPost = (data) => {
  const {
    idUser,
    type,
    images,
    animalsDetails,
    services,
    shopDetails,
    veterinaryDetails,
  } = data;
  const form = new FormData();

  form.append("idUser", idUser);
  form.append("type", type);
  form.append("animalsDetails", JSON.stringify(animalsDetails));
  form.append("services", JSON.stringify(services));
  form.append("shopDetails", JSON.stringify(shopDetails));
  form.append("veterinaryDetails", JSON.stringify(veterinaryDetails));

  images.forEach((image) => {
    form.append("images", image);
  });

  return (dispatch) => {
    dispatch(uiStartLoading());
    fetch(`${API.APP_DOMAIN}/setPost`, {
      method: "POST",
      headers: {
        "content-type": "multipart/form-data",
      },
      body: form,
    })
      .catch((err) => {
        dispatch(uiStopLoading());
      })
      .then((parsedRes) => {
        if (parsedRes) {
          dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            dispatch(setPosts(data));
          });
        }
      })
      .catch((err) => {
        console.warn(err);
      });
  };
};

export const setPosts = (value) => {
  return {
    type: SET_POST,
    setPost: value,
  };
};

export const setPostsReset = (value) => {
  return {
    type: SET_POST,
    setPost: value,
  };
};
