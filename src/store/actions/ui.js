/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Hayfa MRAD
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Mon Apr 29 2019
 */

import { UI_START_LOADING, UI_STOP_LOADING } from "./actionsType";
export const uiStartLoading = () => {
  return {
    type: UI_START_LOADING,
  };
};
export const uiStopLoading = () => {
  return {
    type: UI_STOP_LOADING,
  };
};
