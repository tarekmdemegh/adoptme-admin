import {
  USER_SIGN_UP,
  USER_SIGN_IN,
  USER_INFO,
  PROFIL_UPDATE,
} from "./actionsType";

////// UPDATE

// export const updateProfile = (data) => {
//   //("here !");

//   const {
//     userId,
//     FullName,
//     email,
//     age,
//     adress,
//     mobile,
//     facebook,
//     whatsapp,
//   } = data;
//   //("ssss", data);
//   return (dispatch) => {
//     fetch(`${API.APP_DOMAIN}/updatetUserInfo`, {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//         // "x-access-token": localStorage.getItem("token"),
//       },
//       body: JSON.stringify({
//         userId,
//         FullName,
//         email,
//         age,
//         adress,
//         mobile,
//         facebook,
//         whatsapp,
//       }),
//     })
//       // .catch(err => {
//       //   dispatch(uiStopLoading());
//       // })
//       .then((parsedRes) => {
//         if (parsedRes) {
//           // dispatch(uiStopLoading());
//           parsedRes.json().then((data) => {
//             dispatch(profilUpdate(data));
//           });
//         }
//       });
//   };
// };

// export const profilUpdate = (value) => {
//   return {
//     type: PROFIL_UPDATE,
//     profilUpdate: value,
//   };
// };

// ////// END OF UPDATE

// //

// export const getUserInfo = (data) => {
//   const { userId } = data;

//   return (dispatch) => {
//     fetch(`${API.APP_DOMAIN}/getUserInfo`, {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//         // "x-access-token": localStorage.getItem("token"),
//       },
//       body: JSON.stringify({
//         userId,
//       }),
//     })
//       // .catch(err => {
//       //   dispatch(uiStopLoading());
//       // })
//       .then((parsedRes) => {
//         if (parsedRes) {
//           // dispatch(uiStopLoading());
//           parsedRes.json().then((data) => {
//             dispatch(userInfo(data));
//           });
//         }
//       });
//   };
// };

// export const userInfo = (value) => {
//   return {
//     type: USER_INFO,
//     userInfo: value,
//   };
// };
// export const userInfoReset = (value) => {
//   return {
//     type: USER_INFO,
//     userInfo: value,
//   };
// };
//
export const AdminSignUp = (data) => {
  const { name, lastname, email, password } = data;

  return (dispatch) => {
    fetch(`${process.env.REACT_APP_DOMAIN}/signupAdmin`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // "x-access-token": localStorage.getItem("token"),
      },
      body: JSON.stringify({
        name,
        lastname,
        email,
        password,
      }),
    })
      // .catch(err => {
      //   dispatch(uiStopLoading());
      // })
      .then((parsedRes) => {
        if (parsedRes) {
          // dispatch(uiStopLoading());
          parsedRes.json().then(async (data) => {
            if (data && data.status === "success") {
              await localStorage.setItem("token", data.token);
              await localStorage.setItem("email", data.data.email);
              await localStorage.setItem("mobile", data.data.mobile);

              await localStorage.setItem("id", data.data._id);
              dispatch(setUserSignUp(data));
            } else {
              dispatch(setUserSignUp(data));
            }
          });
        }
      });
  };
};
export const setUserSignUp = (value) => {
  return {
    type: USER_SIGN_UP,
    userInfoData: value,
  };
};

// export const userSignIn = (data) => {
//   const { email, password } = data;
//   return (dispatch) => {
//     fetch(`${API.APP_DOMAIN}/signin`, {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//         // "x-access-token": localStorage.getItem("token"),
//       },
//       body: JSON.stringify({
//         email,
//         password,
//       }),
//     })
//       // .catch(err => {
//       //   dispatch(uiStopLoading());
//       // })
//       .then((parsedRes) => {
//         if (parsedRes) {
//           // dispatch(uiStopLoading());

//           parsedRes.json().then(async (data) => {
//             //("aa", data);
//             if (data && data.status === "success") {
//               await AsyncStorage.setItem("token", data.token);
//               await AsyncStorage.setItem("email", data.data.email);
//               await AsyncStorage.setItem("id", data.data._id);
//               await AsyncStorage.setItem("mobile", data.data.mobile);
//               //("oui user found");
//               dispatch(setUserSignIn(data));
//             } else {
//               //("oui user not found");

//               dispatch(setUserSignIn(data));
//             }
//           });

//           //   parsedRes.json().then(async (data) => {
//           //     await AsyncStorage.setItem("token", data.token);
//           //     await AsyncStorage.setItem("email", data.data.email);
//           //     await AsyncStorage.setItem("id", data.data._id);

//           //     dispatch(setUserSignIn(data));
//           //   });
//         }
//       })
//       .catch((err) => {
//         console.warn(err);
//       });
//   };
// };

// export const setUserSignUp = (value) => {
//   return {
//     type: USER_SIGN_UP,
//     userInfoData: value,
//   };
// };
// export const resetUserSignUp = (value) => {
//   return {
//     type: USER_SIGN_UP,
//     userInfoData: value,
//     userInfoDataSign: value,
//   };
// };

// export const setUserSignIn = (value) => {
//   //(" %c  red => ", value);
//   return {
//     type: USER_SIGN_IN,
//     userInfoDataSign: value,
//   };
// };
