export { AdminSignUp } from "./auth";
export {
  setPost,
  setPostsReset,
  posts,
  postDetails,
  setpostDetailsReset,
} from "./posts";
export { uiStartLoading, uiStopLoading } from "./ui";

export { setFavories, getFavories } from "./favories";

export { setNotifs, getNotifs } from "./notificaion";
