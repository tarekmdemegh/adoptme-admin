export const USER_SIGN_UP = "USER_SIGN_UP";
export const USER_SIGN_IN = "USER_SIGN_IN";

export const SET_POST = "SET_POST";
export const USER_INFO = "USER_INFO";

export const UI_START_LOADING = "UI_START_LOADING";
export const UI_STOP_LOADING = "UI_STOP_LOADING";

export const GET_POST = "GET_POST";
export const GET_POST_DETAILS = "GET_POST_DETAILS";

export const SET_FAVORIES = "SET_FAVORIES";
export const LIST_FAVORIES = "LIST_FAVORIES";

export const PROFIL_UPDATE = "PROFIL_UPDATE";

export const SET_NOTIFS = "SET_NOTIFS";
export const LIST_NOTIFS = "LIST_NOTIFS";
