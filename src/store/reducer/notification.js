import { SET_NOTIFS, LIST_NOTIFS } from "../actions/actionsType";

const initialState = {
  setNotifs: [],
  ListNotifs: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_NOTIFS:
      return {
        ...state,
        setNotifs: action.setNotifs,
      };
    case LIST_NOTIFS:
      return {
        ...state,
        ListNotifs: action.ListNotifs,
      };

    default:
      return state;
  }
};

export default reducer;
