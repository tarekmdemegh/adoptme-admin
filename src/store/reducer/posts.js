import { SET_POST, GET_POST, GET_POST_DETAILS } from "../actions/actionsType";

const initialState = {
  setPost: [],
  getPosts: [],
  getPostDetails: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_POST:
      return {
        ...state,
        setPost: action.setPost,
      };
    case GET_POST:
      return {
        ...state,
        getPosts: action.getPosts,
      };
    case GET_POST_DETAILS:
      return {
        ...state,
        getPostDetails: action.getPostDetails,
      };
    default:
      return state;
  }
};

export default reducer;
