import { USER_SIGN_UP, USER_INFO, USER_SIGN_IN } from "../actions/actionsType";

const initialState = {
  userInfoData: [],
  userInfo: [],
  userInfoDataSign: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_INFO:
      return {
        ...state,
        userInfo: action.userInfo,
      };
    case USER_SIGN_UP:
      return {
        ...state,
        userInfoData: action.userInfoData,
      };
    case USER_SIGN_IN:
      return {
        ...state,
        userInfoDataSign: action.userInfoDataSign,
      };

    default:
      return state;
  }
};

export default reducer;
