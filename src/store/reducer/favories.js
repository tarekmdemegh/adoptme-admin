import { SET_FAVORIES, LIST_FAVORIES } from "../actions/actionsType";

const initialState = {
  setFavoeris: [],
  ListFavoeris: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FAVORIES:
      return {
        ...state,
        setFavoeris: action.setFavoeris,
      };
    case LIST_FAVORIES:
      return {
        ...state,
        ListFavoeris: action.ListFavoeris,
      };

    default:
      return state;
  }
};

export default reducer;
