import React, { Component, Suspense } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Switch } from "react-router";

import Login from "./pages/login";
import SignUp from "./pages/signup";
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="contapp">
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/signup" component={SignUp} />

              {/* <Route exact path="/home" component={Home} /> */}
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
