import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  titleList: {
    color: "black",
    marginTop: 10,
    marginBottom: 10,
    fontFamily: "MontserratBold",
  },
  container: {
    flex: 3,
  },
  titleContainer: {
    flex: 1,
    justifyContent: "center",
    paddingLeft: 20,
  },
  title: {
    fontFamily: "MontserratBold",
    fontSize: 50,
  },
  subTitle: {
    fontSize: 15,
    fontFamily: "MontserratRegular",
  },
  itemsContainer: {
    flex: 1,
    height: 250,
  },
  box1: {
    width: "40%",
    height: 90,
    borderRadius: 10,
    backgroundColor: "#4a37ff",
    justifyContent: "center",
    alignItems: "center",
  },
  box2: {
    width: "40%",
    height: 90,
    borderRadius: 10,
    backgroundColor: "#ffce56",
    justifyContent: "center",
    alignItems: "center",
  },
  box3: {
    width: "40%",
    height: 90,
    borderRadius: 10,
    backgroundColor: "#40dcc7",
    justifyContent: "center",
    alignItems: "center",
  },
  box4: {
    width: "40%",
    height: 90,
    borderRadius: 10,
    backgroundColor: "#ff7c59",
    justifyContent: "center",
    alignItems: "center",
  },
  row1: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 20,
  },
  iconsItem: {
    width: 40,
    height: 40,
  },
  textItem: {
    color: "#fff",
  },
  listContainer: {
    flex: 3,
    marginLeft: 10,
  },
  nameItem: {
    fontSize: 15,
    fontFamily: "MontserratBold",
  },
  nameItemEvent: {
    fontSize: 13,
    fontFamily: "MontserratBold",
    marginTop: 10,
    marginLeft: 5,
  },
  adressItemEvent: {
    fontSize: 12,
    fontFamily: "MontserratSemiBold",
    marginTop: 10,
    marginLeft: 5,
  },
  DateItemEvent: {
    fontSize: 13,
    fontFamily: "MontserratRegular",
    marginTop: 10,
    marginRight: 5,
  },
  genderItem: {
    fontSize: 15,
    fontFamily: "MontserratRegular",
    marginTop: 10,
  },
  card: {
    width: 290,
    height: 150,
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-start",
    borderRadius: 20,
    marginRight: 20,
    overflow: "hidden",
    borderColor: "#999",
    borderWidth: 0.5,
    backgroundColor: "#FFF",
    elevation: 4,
    overflow: "hidden",
    flex: 3,
  },
  cardShop: {
    width: 270,
    height: 270,
    backgroundColor: "#fff",
    flexDirection: "column-reverse",
    justifyContent: "space-between",
    alignItems: "center",
    borderRadius: 20,
    marginRight: 20,
    overflow: "hidden",
    borderColor: "#999",
    borderWidth: 0.5,
    backgroundColor: "#FFF",
    elevation: 4,
    overflow: "hidden",
    flex: 3,
  },
  textCardShop: {
    display: "flex",
    flexDirection: "row",
    flex: 2,
    justifyContent: "flex-start",
    height: "90%",
    paddingLeft: 10,
    paddingTop: 5,
    position: "relative",
  },
  textCard: {
    display: "flex",
    flexDirection: "column",
    flex: 2,
    justifyContent: "flex-start",
    height: "90%",
    paddingLeft: 10,
    paddingTop: 5,
    position: "relative",
  },
  nameItemShop: {
    fontSize: 15,
    fontFamily: "MontserratBold",
    marginTop: 10,
    marginLeft: 25,
    color: "#fff",
  },
  textCardEvent: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    borderTopColor: "#40dcc7",
    borderTopWidth: 13,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 2,
    backgroundColor: "#40dcc7",

    width: "100%",
  },
  btnShowDetail: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 10,
    paddingLeft: 10,
    backgroundColor: "#40dcc7",
    borderRadius: 20,
    width: "50%",
    justifyContent: "center",
    display: "flex",
    alignItems: "center",
    color: "#fff",
  },
  backContainer: {
    width: 70,
    height: 70,
    backgroundColor: "#4a37ff",
    borderBottomRightRadius: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    zIndex: 9,
  },
});
