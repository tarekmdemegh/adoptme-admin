export const fakeDataEvent = [
  {
    id: 1,
    title: "Papadopoulos & Sons",
    adress: "61161 Hovde Street",
    date: "11/2/2019",
    image: "http://dummyimage.com/237x130.png/ff4444/ffffff",
  },
  {
    id: 2,
    title: "Tere Bin Laden",
    adress: "82 Clyde Gallagher Plaza",
    date: "4/22/2020",
    image: "http://dummyimage.com/105x194.png/ff4444/ffffff",
  },
  {
    id: 3,
    title: "Pyx, The",
    adress: "66 Summerview Terrace",
    date: "6/6/2019",
    image: "http://dummyimage.com/109x157.png/dddddd/000000",
  },
  {
    id: 4,
    title: "Comin' at Ya!",
    adress: "22146 Northwestern Crossing",
    date: "12/2/2019",
    image: "http://dummyimage.com/250x129.png/dddddd/000000",
  },
  {
    id: 5,
    title: "Rabbit à la Berlin (Królik po berlinsku)",
    adress: "48716 Tomscot Hill",
    date: "4/18/2020",
    image: "http://dummyimage.com/106x142.png/ff4444/ffffff",
  },
];
